import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    height: 100,
    paddingTop: 60,
    paddingLeft: 40,
    width: '100%',
    backgroundColor: 'lightgray',
  },
});
