import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';

const Footer = (props) => (
  <View style={styles.header}>
    <Text>{ props.counter }</Text>
    <View>
      <Text>{ props.user.name }</Text>
      <Text>{ props.user.age }</Text>
    </View>
    <View>
      <Text>
        The current degree is:
        { props.currentDegree }
      </Text>
    </View>
  </View>
);

const mapStateToProps = ({ counter, user, weather }) => ({ counter, user, currentDegree: weather });

export default connect(mapStateToProps)(Footer);
