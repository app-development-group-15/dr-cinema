import { StyleSheet } from 'react-native';
import { listContainer, itemBg } from '../../styles/colors';

export default StyleSheet.create({
  movieContainer: {
    flexDirection: 'row',
    marginTop: 1,
    paddingLeft: 8,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: listContainer,
    borderRadius: 10,
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    flexDirection: 'row',
    flexWrap: 'wrap',
    paddingRight: 8,
    paddingLeft: 8,
  },
  image: {
    width: 140,
    height: 200,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 1,
  },
  genres: {
    paddingTop: 3,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  infoText: {
    fontSize: 12,
    margin: 3,
    backgroundColor: 'white',
  },
  info: {

    flexDirection: 'row',
    margin: 2,
  },
  buyTicket: {
    // justifyContent: 'center',
    // alignItems: 'center',
    fontSize: 20,
    paddingLeft: 10,
    fontWeight: 'bold',
    paddingTop: 8,
    alignItems: 'center',
  },
  buyTicketContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  ticketsContainer: {
    margin: 3,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 20,
    paddingLeft: 20,
    borderRadius: 10,
    backgroundColor: itemBg,
  },

});
