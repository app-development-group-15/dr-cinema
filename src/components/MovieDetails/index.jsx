import React from 'react';
import {
  View,
  FlatList,
  Text,
  Image,
  TouchableHighlight,
  Linking,
} from 'react-native';
import styles from './styles';

class MovieDetails extends React.Component {
  getShowtimes(cinemaId, movie) {
    const moviesFilter = movie.showtimes.filter((c) => c.cinema.id == cinemaId);
    return moviesFilter[0].schedule;
  }

  render() {
    const { cinemaId, movie } = this.props;
    console.log(movie);
    const schedule = this.getShowtimes(cinemaId, movie);
    return (
      <View>
        <View style={styles.titleContainer}>
          <Text style={styles.title}>{movie.title}</Text>
        </View>
        <View style={styles.movieContainer}>
          <Image style={styles.image} source={{ uri: movie.poster }} resizeMode="cover" />
          <View style={styles.textContainer}>
            <Text>Description</Text>
            <Text>{movie.plot}</Text>
            <View style={styles.genres}>
              {
                movie.genres.map((genre) => (
                  <View>
                    <Text style={styles.infoText}>{genre.Name}</Text>
                  </View>
                ))
              }
            </View>
            <View style={styles.info}>
              <Text style={styles.infoText}>
                {movie.durationMinutes}
                {' '}
                minutes
              </Text>
              <Text style={styles.infoText}>
                release date
                {movie.year}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.buyTicketContainer}>
          <Text style={styles.buyTicket}>Buy a Ticket</Text>
          {
            schedule.map((schedule) => (
              <TouchableHighlight onPress={() => { Linking.openURL(schedule.purchase_url); }}>
                <View style={styles.ticketsContainer}>
                  <View>
                    <Text>{movie.title}</Text>
                    <Text style={styles.genresText}>
                      At:
                      {' '}
                      {schedule.time}
                    </Text>
                  </View>
                </View>
              </TouchableHighlight>
            ))
          }
        </View>
      </View>
    );
  }
}
export default MovieDetails;
