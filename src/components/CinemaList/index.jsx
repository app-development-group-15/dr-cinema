import React from 'react';
import { View, FlatList } from 'react-native';
import {  connect } from 'react-redux';
import CinemaListItem from '../CinemaListItem';

const CinemaList = (props) => {
  const { cinemas } = props;
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        numColumns={1}
        data={cinemas}
        renderItem={({ item }) => (
          <CinemaListItem item={item} />
        )}
        keyExtractor={(cinema) => cinema.id.toString()}
      />
    </View>
  );
};

// const mapStateToProps = ({ cinemas }) => ({ cinemas: cinemas })
function mapStateToProps(state) {
  const { cinemas } = state;
  return { cinemas };
}

export default connect(mapStateToProps)(CinemaList);
