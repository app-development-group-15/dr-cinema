import React from 'react';
import {
  View,
  FlatList,
  Text,
  Image,
  TouchableHighlight,
  Alert,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';

function openVideo(url, navigation) {
  if (url !== '') {
    navigation.navigate('Youtube', { url });
  } else {
    Alert.alert('There is no trailer for this movie');
  }
}
function Movie({
  item: {
    trailers, poster, title, year,
  },
}) {
  const navigation = useNavigation();
  let trailerUrl = '';
  if (trailers[0] !== undefined
    && trailers[0].results[0] !== undefined
    && trailers[0].results[0].url !== undefined) {
    trailerUrl = trailers[0].results[0].url;
  }
  return (
    <TouchableHighlight
      onPress={() => { openVideo(trailerUrl, navigation); }}
    >
      <View style={styles.movieContainer}>
        <Image source={{ uri: poster }} style={styles.thumbnail} />
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={2}>{title}</Text>
          <Text style={styles.year}>{year}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );
}

const renderItem = ({ item }) => <Movie item={item} />;
export default ({ movies }) => (
  <View style={styles.container}>

    <FlatList
      data={movies}
      renderItem={renderItem}
      keyExtractor={(movie) => movie.id.toString()}
    />
  </View>
);
