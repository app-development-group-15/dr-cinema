import { StyleSheet } from 'react-native';
import { itemBg } from '../../styles/colors';

export default StyleSheet.create({
  movieContainer: {
    width: '100%',
    height: 100,
    flexDirection: 'row',
    backgroundColor: itemBg,
    borderRadius: 5,
    marginBottom: 5,
  },
  container: {
  },
  textContainer: {
    paddingLeft: 15,
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
  },
  thumbnail: {
    height: '100%',
    width: 80,
  },
});
