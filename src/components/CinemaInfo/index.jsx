import React from 'react';
import { View, Text, FlatList } from 'react-native';
//import CinemaListItem from '../../components/CinemaListItem';
import { connect } from 'react-redux';
import CinemaInfoMovieItem from '../../components/CinemaInfoMovieItem';
import styles from './styles';


const CinemaInfo = (props) => {
  const { movies, cinema } = props;
  return (

    <View style={{ flex: 1 }}>
      <View style={styles.description} >
        <Text style={styles.name}>{cinema.name}</Text>
        <Text>{cinema.description}</Text>
        <Text>{cinema.phone}</Text>
        <Text>{cinema.website}</Text>
        <Text>{cinema.['address\t']}, {cinema.city}</Text>
      </View>

      <FlatList
        numColumns={1}
          data={ movies }
        renderItem={({ item }) => (
            <CinemaInfoMovieItem item={item} cinema={cinema} />
        )}
        keyExtractor={(movie) => movie.id.toString()}
      />
    </View>

  )
}
// const mapStateToProps = ({ cinemas }) => ({ cinemas: cinemas })
function mapStateToProps(state) {
  const { movies } = state;
  return { movies };
}


  export default connect(mapStateToProps)(CinemaInfo);
