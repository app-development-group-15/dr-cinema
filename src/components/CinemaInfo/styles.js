import { StyleSheet } from 'react-native';
import { listContainer } from '../../styles/colors';

export default StyleSheet.create({
  description: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 1,
    paddingLeft: 8,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: listContainer,
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
    justifyContent: 'center',
  },
});
