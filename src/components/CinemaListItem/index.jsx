import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';
// import { connect } from 'react-redux';
// import { getCinemaMovies } from '../../actions/movieActions';

const CinemaListItem = (props) => {
  const navigation = useNavigation();
  const { item } = props;
  return (
    <TouchableHighlight
      // Should go to details about cinema and viewings screen
      onPress={() => navigation.navigate('CinemaInfoView', { cinema: item })}
    >
      <View style={styles.cinemaContainer}>
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={2}>
            {item.name}
            {' '}
          </Text>
          <Text style={styles.year}>{item.website}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );
};

export default CinemaListItem;
