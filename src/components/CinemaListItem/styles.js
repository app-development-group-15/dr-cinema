import { StyleSheet } from 'react-native';
import { listContainer, itemBg } from '../../styles/colors';

export default StyleSheet.create({
  container: {
    marginTop: 1,
    paddingLeft: 8,
    paddingTop: 15,
    paddingBottom: 15,
    flexDirection: 'row',
    backgroundColor: listContainer,
    borderRadius: 10,
  },
  name: {
    alignItems: 'center',
    fontSize: 20,
    paddingLeft: 10,
  },
  website: {
    fontSize: 14,
    marginLeft: 'auto',
    paddingRight: 15,
    paddingTop: 25,
    textDecorationLine: 'underline',
  },
  cinemaContainer: {
    width: '100%',
    height: 100,
    flexDirection: 'row',
    backgroundColor: itemBg,
    borderRadius: 5,
    marginBottom: 5,
  },
  container: {
    marginBottom: 200,
  },
  textContainer: {
    paddingLeft: 15,
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
  },
  // thumbnail: {
  //   height: '100%',
  //   width: 80,
  // },
});
