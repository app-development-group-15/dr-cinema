import React from 'react';
import {
  Text, View, TouchableHighlight, Image,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';

const CinemaInfoMovieItem = (props) => {
  const navigation = useNavigation();
  const { item, cinema } = props;
  return (
    <TouchableHighlight
      // Should go to details about cinema and viewings screen
      onPress={() => navigation.navigate('MovieDetailsView', { movie: item, cinemaId: cinema.id })}
    >
      <View style={styles.movieContainer}>
        <Image source={{ uri: item.poster }} style={styles.thumbnail} />
        <View style={styles.textContainer}>
          <Text style={styles.title} numberOfLines={2}>{item.title}</Text>
          <Text style={styles.year}>{item.year}</Text>
          <View style={styles.genres}>
            {
              item.genres.map((genre) => (
                <View>
                  <Text style={styles.genresText}>{genre.Name}</Text>
                </View>
              ))
            }
          </View>
        </View>
      </View>
    </TouchableHighlight>
  );
};

export default CinemaInfoMovieItem;
