import { StyleSheet } from 'react-native';
import { itemBg } from '../../styles/colors';

export default StyleSheet.create({
  movieContainer: {
    width: '100%',
    height: 100,
    flexDirection: 'row',
    backgroundColor: itemBg,
    borderRadius: 5,
    marginBottom: 5,
  },
  container: {
    marginBottom: 200,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 15,
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
  },
  thumbnail: {
    height: '100%',
    width: 80,
  },
  genres: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  genresText: {
    fontSize: 12,
    margin: 3,
  },
});
