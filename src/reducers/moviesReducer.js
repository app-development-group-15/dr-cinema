import * as constants from '../constants';

export default function upComingMovies(state = 0, action) {
  switch (action.type) {
    case constants.GET_UP_COMING_MOVIES: console.log(action.payload); return action.payload;
    default: return state;
  }
}
