import * as constants from '../constants';

export default function showings(state = {}, action) {
  switch (action.type) {
    case constants.GET_CINEMA_SHOWINGS: return action.payload;
    default: return state;
  }
}
