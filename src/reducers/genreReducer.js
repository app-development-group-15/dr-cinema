import * as constants from '../constants';

export default function genres(state = 0, action) {
  switch (action.type) {
    case constants.GET_GENRES: return action.payload;
    default: return state;
  }
}
