import { combineReducers } from 'redux';
import cinemas from './cinemaReducer';
import movies from './showingsReducer';
import upComingMovies from './moviesReducer';

export default combineReducers({
  cinemas,
  movies,
  upComingMovies,
});
