// import { getMovies } from '../services/apiService';
import { getMovies }  from '../services/apiService';
import * as constants from '../constants';

export const getCinemaMovies = (cinema) => {
  return async dispatch => {
    try {
      const allMovies = await getMovies();
      const moviesInCinema = allMovies.filter(movie => {
        return movie.showtimes.some(s => s.cinema.id === cinema.id)
      })
      dispatch(getAllMoviesSuccess(moviesInCinema))
    } catch (err)  {
      console.log(err)
    }
  };
}

const getAllMoviesSuccess = movies => {
  return {
    type: constants.GET_CINEMA_SHOWINGS,
    payload: movies,
  }
};