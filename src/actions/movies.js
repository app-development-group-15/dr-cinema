import * as constants from '../constants';
import { getUpComingMovies } from '../services/apiService';

const getMoviesSuccess = (upComingMovies) => ({
  type: constants.GET_UP_COMING_MOVIES,
  payload: upComingMovies,
});

export default () => async (dispatch) => {
  try {
    const movies = await getUpComingMovies();
    const sorted = movies.sort((a, b) => (a.year > b.year ? -1 : (a.year) < b.year));
    dispatch(getMoviesSuccess(sorted));
  } catch (err) {
    // temp
  }
};
