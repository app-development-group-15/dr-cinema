// import { getMovies } from '../services/apiService';
import { getCinemas }  from '../services/apiService';
import * as constants from '../constants';

export const getAllCinemas = () => {
    return async dispatch => {
        try {
            const cinemas = await getCinemas();
            const sortedCinemas = cinemas.sort((a, b) => (a.name < b.name ? -1 : (a.name) > b.name))
            dispatch(getCinemasSuccess(sortedCinemas))
        } catch (err)  {
            console.log(err)
        }
    };
}

const getCinemasSuccess = cinemas => {
    return {
        type: constants.GET_CINEMAS,
        payload: cinemas
    }
};
