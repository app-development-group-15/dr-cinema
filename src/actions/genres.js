import { getGenres } from '../services/apiService';
import * as constants from '../constants';

const getGenresSuccess = (genres) => ({
  type: constants.GET_GENRES,
  payload: genres,
});

const getAllGenres = () => async (dispatch) => {
  try {
    const genres = await getGenres();
    dispatch(getGenresSuccess(genres));
  } catch (err) {
    // TODO: Should dispatch an error action
  }
};

export default {
  getAllGenres,
};
