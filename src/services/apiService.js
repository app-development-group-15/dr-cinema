import * as axios from 'axios';

const apiUrl = 'https://api.kvikmyndir.is';

const apiKey = {
  timeOfAcqusition: 1607344015195,
  key: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1ZmNlM2ExYzE2ZjczZjE2NjQ4OThjNzMiLCJnbG9iYWxhZG1pbiI6ZmFsc2UsImFkbWluIjpmYWxzZSwiYWN0aXZlIjp0cnVlLCJmdWxsbmFtZSI6IkZqw7ZsbmlyIMOecmFzdGFyc29uIiwiZW1haWwiOiJmam9sbmlyOTZAeWFob28uY29tIiwidXNlcm5hbWUiOiJub3RhZG9jdG9yIiwicGFzc3dvcmQiOiIkMmEkMDgkM2xnTzZQWG1tbDZrNTlCZFhxWW52dTgySGtHbHdoLktQYmZTVmJDdFJ3bWViNDVKV0REWTIiLCJkb21haW4iOiJleHBvIiwibWVzc2FnZSI6IkkgaW50ZW5kIHRvIHVzZSBpdCBmb3IgZGV2ZWxvcGluZyBhbiBhcHAgZm9yIHNjaG9vbCBpbiB0aGUgY2xhc3Mgc23DoWZvcnJpdHVuLiBOb24tY29tbWVyY2lhbGx5LiBPbmx5IGZvciBlZHVjYXRpb25hbCBwdXJwb3Nlcy4iLCJpYXQiOjE2MDc2NzQyMDMsImV4cCI6MTYwNzc2MDYwM30.Qwx91GO0Mo2_2xMjHz98a41zyQnSHK1llteZ01ArYkc',
};
function getCurrentApiKey() {
  return apiKey.key;
}

async function getDataFromEndpoint(endpoint) {
  const header = {
    'x-access-token': getCurrentApiKey(),
  };
  const response = await axios.get(`${apiUrl}/${endpoint}`, { headers: header });
  return response.data;
}

const getMovies = () => getDataFromEndpoint('movies');

const getGenres = () => getDataFromEndpoint('genres');

const getUpComingMovies = () => getDataFromEndpoint('upcoming');

const getCinemas = () => getDataFromEndpoint('theaters');

export {
  getMovies,
  getGenres,
  getUpComingMovies,
  getCinemas,
};
