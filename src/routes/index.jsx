import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { createStackNavigator } from '@react-navigation/stack';

// import Cinemas from '../views/Cinemas';
import Movies from '../views/Movies';
import CinemaListView from '../views/CinemaListView';
import CinemaInfoView from '../views/CinemaInfoView';
import MovieDetailsView from '../views/MovieDetailsView';
import YoutubeVideo from '../views/YoutubeVideo';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
function CinemaNavigator() {
  return (
    <Stack.Navigator>
      {/* <Stack.Screen name="Home" component={Cinemas} /> */}
      <Stack.Screen name="CinemaListView" component={CinemaListView} />
      <Stack.Screen name="CinemaInfoView" component={CinemaInfoView} />
      <Stack.Screen name="MovieDetailsView" component={MovieDetailsView} />
    </Stack.Navigator>
  );
}

function UpComingMovieNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="UpComing"
        component={Movies}
        options={{ title: 'Up Coming Movies' }}
      />
      <Stack.Screen
        name="Youtube"
        component={YoutubeVideo}
        options={{ title: '' }}
      />
    </Stack.Navigator>
  );
}
export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === 'Cinemas') {
              iconName = 'theater';
            } else if (route.name === 'Movies') {
              iconName = focused ? 'movie-open' : 'movie-open-outline';
            }

            // You can return any component that you like here!
            return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Cinemas" component={CinemaNavigator} />
        <Tab.Screen name="Movies" component={UpComingMovieNavigator} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
