import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import CinemaList from '../../components/CinemaList';
import {  getAllCinemas } from '../../actions/cinemaActions';


class CinemaListView extends React.Component {
  componentDidMount() {
    const { getAllCinemas } = this.props;
    getAllCinemas();
    this.props.navigation.setOptions({
      title: 'Cinemas',
          // Dispatch action when component mounts
    })
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <CinemaList />
      </View>
    )
  }
};
// const mapStateToProps = ({ cinemas }) => ({ cinemas: cinemas })
function mapStateToProps(state) {
  const { cinemas } = state;
  return { cinemas };
}

export default connect(mapStateToProps, { getAllCinemas })(CinemaListView);