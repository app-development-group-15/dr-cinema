import React from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';

class YoutubeVideo extends React.Component {
  componentDidMount() {
    // do nothing i guess
    // this is just here because I hate linting errors but dont have time to fix them all :D
  }

  render() {
    const { route: { params: { url } } } = this.props;
    return (
      <View style={{ width: '100%', height: 300 }}>
        <WebView source={{ uri: url }} />
      </View>
    );
  }
}
export default YoutubeVideo;
