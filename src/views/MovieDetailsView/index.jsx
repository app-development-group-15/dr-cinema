import React from 'react';
import MovieDetails from '../../components/MovieDetails';
import { Text, View } from 'react-native';

class MovieDetailsView extends React.Component {
  componentDidMount() {
    const { movie } = this.props.route.params;
    this.props.navigation.setOptions({
      title: 'Movie Showings',
      // Dispatch action when component mounts
    })
  }
  render() {
    const { movie, cinemaId } = this.props.route.params;
    return (
      <View>
        <MovieDetails movie={movie} cinemaId={cinemaId} />
      </View>
    )
  }
};

export default MovieDetailsView;