import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  title: {
    paddingTop: 15,
    fontSize: 25,
    color: 'tomato',
  },
});
