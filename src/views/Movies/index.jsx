import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import MovieList from '../../components/MovieList';

const Movies = ({ upComingMovies }) => {
  console.log('MOVIE VIEW', upComingMovies);
  return (
    <View style={styles.container}>
      <MovieList movies={upComingMovies} />
    </View>
  );
};
// const mapStateToProps = ({ movies }) => ({ movies });
function mapStateToProps(state) {
  const { upComingMovies } = state;
  return { upComingMovies };
}

export default connect(mapStateToProps)(Movies);
