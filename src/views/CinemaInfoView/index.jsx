import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import CinemaInfo from '../../components/CinemaInfo';
import { getCinemaMovies } from '../../actions/showingsActions';


class CinemaInfoView extends React.Component {
  componentDidMount() {
    const { getCinemaMovies } =  this.props;
    const { cinema } = this.props.route.params
    getCinemaMovies(cinema);
    this.props.navigation.setOptions({
      title: cinema.name,
      // Dispatch action when component mounts
    })
  }
  render() {
    const { cinema } = this.props.route.params
    return (
      <View style={{ flex: 1 }}>
        <CinemaInfo cinema={cinema} />
      </View>
    )
  }
};
// const mapStateToProps = ({ cinemas }) => ({ cinemas: cinemas })
// function mapStateToProps(state) {
//   const { movies } = state;
//   return { movies };
// }

export default connect(null, { getCinemaMovies })(CinemaInfoView);