import { StyleSheet, Dimensions } from 'react-native';

const { width: winWidth } = Dimensions.get('window');

export default StyleSheet.create({
  header: {
    backgroundColor: 'black',
    height: 10,
  },
  button: {
    paddingRight: 15,
    paddingLeft: 15,
    paddingBottom: 10,
    color: 'blue',
  },
  settingsItem: {
    width: winWidth,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: 'black',
  },
});
