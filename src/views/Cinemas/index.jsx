import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import styles from './styles';

class Settings extends React.Component {
  componentDidMount() {
    this.props.navigation.setOptions({
      title: 'Cinemas stack navigation',
    });
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <TouchableHighlight
          style={styles.settingsItem}
        >
          <Text>Delete all contacts</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={styles.settingsItem}
        >
          <Text>Import local contacts</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
export default Settings;
