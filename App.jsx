import React from 'react';
import { Provider, connect } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from './src/reducers';
import Routes from './src/routes';
import getUpComingMovies from './src/actions/movies';

class SubApp extends React.Component {
  componentDidMount() {
    this.props.getUpComingMovies();
  }

  render() {
    return (
      <Routes />
    );
  }
}

const ConnectedSubApp = connect(null, { getUpComingMovies })(SubApp);
export default function App() {
  return (
    <Provider store={createStore(reducers, applyMiddleware(thunk))}>
      <ConnectedSubApp />
    </Provider>
  );
}
