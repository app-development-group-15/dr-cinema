import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {  Provider, connect } from 'react-redux';
import {  createStore, applyMiddleware } from 'redux';
import cinemaReducer from './src/reducers/cinemaReducer';
import CinemaWrapper from './src/components/CinemaWrapper';
import Cinema from './src/components/Cinema';
import thunk from 'redux-thunk';
import {  getAllCinemas } from './src/actions/cinemaActions';

class SubApp extends React.Component {
    componentDidMount() {
        // Dispatch action when component mounts
        this.props.getAllCinemas();
    }
    render() {
        return (
            <View style={styles.container}>
                <CinemaWrapper>
                    <Cinema />
                </CinemaWrapper>
            </View>
        )
    }
}

const ConnectedSubApp = connect(null, { getAllCinemas })(SubApp)


export default function App() {
    return (
        <Provider store={createStore(cinemaReducer, applyMiddleware(thunk))} >
            <ConnectedSubApp />
        </Provider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
